module RPGM

go 1.22.1

require (
	github.com/goccy/go-json v0.10.2
	github.com/minio/simdjson-go v0.4.5
)

require (
	github.com/bas24/googletranslatefree v0.0.0-20231117033553-f5859fe54d30 // indirect
	github.com/klauspost/compress v1.15.15 // indirect
	github.com/klauspost/cpuid/v2 v2.2.3 // indirect
	golang.org/x/sys v0.0.0-20220704084225-05e143d24a9e // indirect
)
