# GameMaker Studio 2 IDE Translation.
[![FOSSA Status](https://app.fossa.com/api/projects/git%2Bgithub.com%2FGamemakerChina%2Fgms2translation.svg?type=shield)](https://app.fossa.com/projects/git%2Bgithub.com%2FGamemakerChina%2Fgms2translation?ref=badge_shield)


Currently only Simplified Chinese and English.

汉化文件位于仓库的 [gh-pages](https://github.com/GamemakerChina/gms2translation/tree/gh-pages) 分支

Require [omegat-gms2-ide-csv-filter](https://github.com/GamemakerChina/omegat-gms2-ide-csv-filter) plugin.

## Usage

### IDE 目录

根据不同来源不同版本，IDE 目录可能有以下几个名称：

```text
GameMaker Studio 2
GameMaker Studio 2-Beta
GameMaker
GameMaker-Beta
GameMaker-LTS
GameMaker Studio 2 Desktop (限 Steam)
GameMaker Studio 2 Web (限 Steam)
GameMaker Studio 2 Mobile (限 Steam)
GameMaker Studio 2 UWP (限 Steam)
```

### 2023.4.0.84 - 最新

下载 `chinese.zip` 并解压，其中 `chinese_startup.csv` 粘贴到 IDE 目录的 `Language` 文件夹下，`Chinese` 文件夹内的文件（包含该文件夹）粘贴并覆盖到 IDE 目录的 `Plugins\GMBaseIDELanguages\Languages` 文件夹下。

### 2.0.6.136 - 2023.2.0.71

下载 `chinese.csv` 和 `chinese_dnd.csv`（如果有)，粘贴到 IDE 目录的 `Language` 文件夹下。

## Machine Translation Plugin reference 

 - Google Translation (built-in or [https://sourceforge.net/projects/omegat-gt-without-api-key/](https://sourceforge.net/projects/omegat-gt-without-api-key/))
 - Microsoft Translation (built-in)
 - DeepL (built-in)
 - Tencent Translation ([https://github.com/GamemakerChina/omegat-tencent-plugin](https://github.com/GamemakerChina/omegat-tencent-plugin))
 - Caiyun ([https://gitee.com/xffish/omegat-caiyun-interpreter-plugin](https://gitee.com/xffish/omegat-caiyun-interpreter-plugin))
 - Youdao Translation ([https://gitee.com/xffish/omegat-youdao-plugin](https://gitee.com/xffish/omegat-youdao-plugin))

## License
[![FOSSA Status](https://app.fossa.com/api/projects/git%2Bgithub.com%2FGamemakerChina%2Fgms2translation.svg?type=large)](https://app.fossa.com/projects/git%2Bgithub.com%2FGamemakerChina%2Fgms2translation?ref=badge_large)
